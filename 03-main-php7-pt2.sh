#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

cat >/etc/mysql/mysql.conf.d/99-dev.cnf << EOF1
[mysqld]
bind-address = 0.0.0.0
EOF1

echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu $(lsb_release -sc) main" |  tee /etc/apt/sources.list.d/nginx-stable.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C
apt update -y
apt install -y \
    nginx redis-server \
    php7.0-fpm php7.0-cli php7.0-mcrypt php7.0-curl php7.0-gd sqlite php7.0-sqlite
# apt install phpunit -y # need php 7 version
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

cat >/etc/php/7.0/fpm/conf.d/99-dev-machine.ini << EOF2
upload_max_filesize=25M
post_max_size=25M
display_startup_errors=1
display_errors=1
EOF2

cat >/etc/php/7.0/cli/conf.d/99-dev-machine.ini << EOF3
upload_max_filesize=25M
post_max_size=25M
display_startup_errors=1
display_errors=1
EOF3

apt -f install
apt -y autoremove
apt -y autoclean
apt -y clean

chown $SUDO_USER:$SUDO_USER /var/run/php/php7.0-fpm.sock
# echo "nano /etc/php/7.0/fpm/pool.d/www.conf"
sed -i "s/www-data/$SUDO_USER/g" /etc/php/7.0/fpm/pool.d/www.conf

# touch /etc/apt/sources.list.d/pgdg.list
# echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
# wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
#   apt-key add -
# apt update
#
# apt install -y python-software-properties
# add-apt-repository ppa:ondrej/php
#
# apt update
# apt install -y php7.0
# apt install -y \
#     php-pear php7.0-gd php7.0-dev \
#     php7.0-cli php7.0-dev php7.0-fpm \
#     php7.0-mysql php7.0-pgsql php7.0-sqlite3
# pear channel-discover pear.phpmd.org
# pear channel-discover pear.pdepend.org
# pear install --alldeps phpmd/PHP_PMD
#
# wget http://cs.sensiolabs.org/get/php-cs-fixer.phar -O /usr/local/bin/php-cs-fixer
# chmod a+x /usr/local/bin/php-cs-fixer

# apt install -y \
#     mysql-server \
#     php7.0-mysql php7.0-mysqlnd php7.0-mysqlnd
# mysql_install_db && /usr/bin/mysql_secure_installation
