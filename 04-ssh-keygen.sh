#!/bin/bash
if [[ !$(/usr/bin/id -u) -ne 0 ]]; then
    echo "Do Not Run This As Root"
    exit
fi

set -e
set -x #echo on

echo "Type in your email address [ENTER]:"
read EMAIL

# https://github.com/so-fancy/diff-so-fancy
# cat >~/.gitconfig <<EOF
# [alias]
# 	dsf = "!f() { [ \"$GIT_PREFIX\" != \"\" ] && cd "$GIT_PREFIX"; git diff --color $@ | diff-so-fancy | less --tabs=4 -RFX; }; f"
# EOF

mkdir ~/.ssh || true;
touch ~/.ssh/config
cat >~/.ssh/config <<EOF
# This reduces your number of "Broken Pipe" error messages
# Which is mostly a WiFi/high packet loss issue
ServerAliveInterval 100
EOF

ssh-keygen -t rsa -C "$EMAIL";
ssh-agent -s;
ssh-add ~/.ssh/id_rsa;
echo "Add that key to your github, optionally give it to your supervisor if you are approved to ssh into any servers
"
cat ~/.ssh/id_rsa.pub
echo "
"
