#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

echo "SUDO_USER: $SUDO_USER"

apt remove apache2 -y

add-apt-repository ppa:webupd8team/sublime-text-3
apt update -y
apt upgrade -y
apt dist-upgrade -y
apt autoremove

apt install -y \
    p7zip-rar p7zip-full unace unrar zip unzip sharutils rar uudeview mpack arj cabextract file-roller \
    tmux screen \
    git curl \
    vim nano screen geany geany-plugins meld \
    gcc g++ \
    ack-grep silversearcher-ag

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt install nodejs -y
npm install -g \
    gulp gulp-less gulp-sass gulp-minify-css gulp-concat gulp-uglify gulp-rename gulp-notify gulp-phpunit gulp-replace gulp-if gulp-filter gulp-through gulp-inject gulp-add-src gulp-ll gulp-plumber \
    gulp-babel babel-preset-es2015 babel-preset-stage-0 \
    event-stream bower node-sass sequelize phplint \
    diff-so-fancy \
    nodemon eslint

apt install -y \
    dnsutils linkchecker \
    sublime-text-installer chromium-browser

# echo "yourhostname" > /etc/hostname
nano /etc/hostname
# echo "install Postgre 9.5+ http://www.postgresql.org/download/linux/ubuntu/ "

touch /etc/apt/sources.list.d/pgdg.list
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  apt-key add -
apt update

# https://github.com/so-fancy/diff-so-fancy
cat >~/.gitconfig <<EOF4
[alias]
    dsf = "!f() { [ \"$GIT_PREFIX\" != \"\" ] && cd "$GIT_PREFIX"; git diff --color $@ | diff-so-fancy | less --tabs=4 -RFX; }; f"
EOF4
