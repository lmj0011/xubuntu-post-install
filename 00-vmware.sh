#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

# http://www.liberiangeek.net/2015/06/installing-vmware-guest-tools-in-ubuntu-15-04/
apt update -y
apt dist-upgrade -y
apt autoremove

# vmtools gets installed automatically from this command
apt install open-vm-tools open-vm-tools-dkms

# sudo -H -u $SUDO_USER -s bash -c "tar -xvf /media/$SUDO_USER/\"VMware Tools\"/VMwareTools*.gz -C /tmp"
# /tmp/vmware-tools-distrib/vmware-install.pl -d
