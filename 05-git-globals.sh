#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

echo "Type in your email address [ENTER]:"
read EMAIL
# EMAIL='m@m.com'

echo "Type in your name [ENTER]:"
read NAMEOFUSER
# NAMEOFUSER='michael'

sudo -H -u $SUDO_USER -s bash << EOF
    git config --global pull.rebase true
    git config --global user.email "$EMAIL"
    git config --global user.name  "$NAMEOFUSER"
EOF

# cat << EOF
#     ssh-keygen -t rsa -C "$EMAIL";
#     ssh-agent -s;
#     ssh-add ~/.ssh/id_rsa;
#     echo "Add that key to your github, optionally give it to your supervisor if you are approved to ssh into any servers"
#     cat ~/.ssh/id_rsa.pub
# EOF
